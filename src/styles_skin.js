const partial = (name, index, radius="none")=> (
  `:host .${name}:nth-child(${index}) {
    background: var(--onscreen-gamepad-${name}${index}-background, #99999933);
    border-radius: var(--onscreen-gamepad-${name}${index}-border-radius, ${radius});
    transform: var(--onscreen-gamepad-${name}${index}-transform);
  }
  :host .${name}:nth-child(${index}).${name}--pressed {
    background: var(--onscreen-gamepad-${name}${index}-pressed-background, #33333333);
  }`
);

export default `
  ${partial("button", 1, "100%")}
  ${partial("button", 2, "100%")}
  ${partial("button", 3, "100%")}
  ${partial("button", 4, "100%")}
  ${partial("button", 5, "100% 0 0 0")}
  ${partial("button", 6, "0 100% 0 0")}
  ${partial("button", 7, "100% 0 100% 0")}
  ${partial("button", 8, "0 100% 0 100%")}
  ${partial("button", 9)}
  ${partial("button", 10)}
  ${partial("button", 11, "100%")}
  ${partial("button", 12, "100%")}
  ${partial("button", 13, "0 0 100% 100%")}
  ${partial("button", 14, "100% 100% 0 0")}
  ${partial("button", 15, "0 100% 100% 0")}
  ${partial("button", 16, "100% 0 0 100%")}
  ${partial("button", 17, "100%")}
  ${partial("axes", 1, "100%")}
  ${partial("axes", 2, "100%")}

  :host .pad {
    background: var(--onscreen-gamepad-pad-background, #99999933);
  }
`;