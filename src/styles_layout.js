export default `
  :host {
    display: inline-block;
    width: 180mm;
    height: 90mm;
  }

  :host>.vnode {
    width: 100%;
    height: 100%;
    position: relative;
  }

  :host .pad,
  :host .buttons,
  :host .axises {
    width: 100%;
    height: 100%;
    position: absolute;
  }

  :host .buttons,
  :host .axises {
    display: grid;
    grid-template-columns: repeat(18, 1fr);
    grid-template-rows: repeat(9, 1fr);
  }

  :host .button:nth-child(1) {
    grid-column: 15/17;
    grid-row: 7/9;
  }

  :host .button:nth-child(2) {
    grid-column: 17/19;
    grid-row: 5/7;
  }

  :host .button:nth-child(3) {
    grid-column: 13/15;
    grid-row: 5/7;
  }

  :host .button:nth-child(4) {
    grid-column: 15/17;
    grid-row: 3/5;
  }

  :host .button:nth-child(5) {
    grid-column: 1/3;
    grid-row: 2/3;
  }

  :host .button:nth-child(6) {
    grid-column: 17/19;
    grid-row: 2/3;
  }

  :host .button:nth-child(7) {
    grid-column: 1/3;
    grid-row: 1/2;
  }

  :host .button:nth-child(8) {
    grid-column: 17/19;
    grid-row: 1/2;
  }

  :host .button:nth-child(9) {
    grid-column: 8/9;
    grid-row: 6/7;
  }

  :host .button:nth-child(10) {
    grid-column: 11/12;
    grid-row: 6/7;
  }

  :host .button:nth-child(11) {
    grid-column: 6/8;
    grid-row: 2/3;
  }

  :host .button:nth-child(12) {
    grid-column: 12/14;
    grid-row: 2/3;
  }

  :host .button:nth-child(13) {
    grid-column: 3/5;
    grid-row: 3/5;
  }

  :host .button:nth-child(14) {
    grid-column: 3/5;
    grid-row: 7/9;
  }

  :host .button:nth-child(15) {
    grid-column: 1/3;
    grid-row: 5/7;
  }

  :host .button:nth-child(16) {
    grid-column: 5/7;
    grid-row: 5/7;
  }

  :host .button:nth-child(17) {
    grid-column: 9/11;
    grid-row: 5/7;
  }

  :host .axes:nth-child(1) {
    grid-column: 6/8;
    grid-row: 8/10;
  }

  :host .axes:nth-child(2) {
    grid-column: 12/14;
    grid-row: 8/10;
  }

  :host .buttons,
  :host .axises {
    pointer-events: none;
  }
  
  :host .buttons__button,
  :host .axises__axes {
    pointer-events: auto;
  }
`;