import Gamepad from "./Gamepad.js";

export default function(mappingType) {
  if (mappingType === "standard") {
    return new Gamepad();
  }
}