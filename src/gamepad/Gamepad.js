export default class {
  constructor() {
    this.buttons = Array.from({ length: 17 }).map(()=> ({ value: 0, pressed: false }));
    this.axes = Array.from({ length: 4 }).map(()=> 0);
    this.connected = false;
    this.id = "";
    this.index = 0;
    this.mapping = "standard";
    this.timestamp = 0;
  }
}