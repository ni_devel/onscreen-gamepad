import styles_layout from "./styles_layout.js";
import styles_skin from "./styles_skin.js";
import styles_vendor from "./styles_vendor.js";
import Vnode from "./Vnode.js";
import createGamepad from "./gamepad/createGamepad.js";

export default class extends HTMLElement {
  constructor() {
    super();
    let shadow = this.attachShadow({mode: "open"});
    shadow.innerHTML = TEMPLATE;

    this.gamepad = createGamepad("standard");
    this.vnode = new Vnode(shadow.querySelector(".vnode"), this.gamepad);
  }

  connectedCallback() {
    this._dispatch("gamepadconnected");
  }

  disconnectedCallback() {
    this._dispatch("gamepaddisconnected");
  }

  _dispatch(name) {
    let event = new CustomEvent(name);
    event.gamepad = this.gamepad;
    window.dispatchEvent(event);
  }
}

const TEMPLATE = `
  <style>
    ${styles_layout}
    ${styles_skin}
    ${styles_vendor}
  </style>
  <div class="vnode"></div>
`;