const { h, app } = require("hyperapp");

export default class {
  constructor(element, gamepad) {
    const state = {
      button0: 0,
      button1: 0,
      button2: 0,
      button3: 0,
      button4: 0,
      button5: 0,
      button6: 0,
      button7: 0,
      button8: 0,
      button9: 0,
      button10: 0,
      button11: 0,
      button12: 0,
      button13: 0,
      button14: 0,
      button15: 0,
      button16: 0,
      axes0: 0,
      axes1: 0,
      axes2: 0,
      axes3: 0,
    }

    const actions = {
      pressed: (name)=> ({ [name]: 1 }),
      released: (name)=> ({ [name]: 0 }),
      update: (state)=> {
        gamepad.buttons.forEach((button, index)=> {
          button.value = state["button" + index];
          button.pressed = !!state["button" + index];
        });

        gamepad.axes.forEach((axes, index)=> {
          axes = state["axes" + index];
        });
      },
    }

    const events = (name, actions)=> ({
      onmousedown: ()=> actions.pressed(name),
      onmouseup: ()=> actions.released(name),
      ontouchstart: ()=> actions.pressed(name),
      ontouchend: ()=> actions.released(name),
    });

    const view = (state, actions) =>
      h("div", { class: "pad", onupdate: ()=> actions.update(state) }, [
        h("div", { class: "pad__buttons buttons" }, [
          h("div", { class: `buttons__button button button--${state.button0?"pressed":"released"}`, ...events("button0", actions) }),
          h("div", { class: `buttons__button button button--${state.button1?"pressed":"released"}`, ...events("button1", actions) }),
          h("div", { class: `buttons__button button button--${state.button2?"pressed":"released"}`, ...events("button2", actions) }),
          h("div", { class: `buttons__button button button--${state.button3?"pressed":"released"}`, ...events("button3", actions) }),
          h("div", { class: `buttons__button button button--${state.button4?"pressed":"released"}`, ...events("button4", actions) }),
          h("div", { class: `buttons__button button button--${state.button5?"pressed":"released"}`, ...events("button5", actions) }),
          h("div", { class: `buttons__button button button--${state.button6?"pressed":"released"}`, ...events("button6", actions) }),
          h("div", { class: `buttons__button button button--${state.button7?"pressed":"released"}`, ...events("button7", actions) }),
          h("div", { class: `buttons__button button button--${state.button8?"pressed":"released"}`, ...events("button8", actions) }),
          h("div", { class: `buttons__button button button--${state.button9?"pressed":"released"}`, ...events("button9", actions) }),
          h("div", { class: `buttons__button button button--${state.button10?"pressed":"released"}`, ...events("button10", actions) }),
          h("div", { class: `buttons__button button button--${state.button11?"pressed":"released"}`, ...events("button11", actions) }),
          h("div", { class: `buttons__button button button--${state.button12?"pressed":"released"}`, ...events("button12", actions) }),
          h("div", { class: `buttons__button button button--${state.button13?"pressed":"released"}`, ...events("button13", actions) }),
          h("div", { class: `buttons__button button button--${state.button14?"pressed":"released"}`, ...events("button14", actions) }),
          h("div", { class: `buttons__button button button--${state.button15?"pressed":"released"}`, ...events("button15", actions) }),
          h("div", { class: `buttons__button button button--${state.button16?"pressed":"released"}`, ...events("button16", actions) }),
        ]),

        h("div", { class: "pad__axises axises"}, [
          h("div", { class: "axises__axes axes" }),
          h("div", { class: "axises__axes axes" }),
        ]),
      ]);

    app(state, actions, view, element);
  }
}